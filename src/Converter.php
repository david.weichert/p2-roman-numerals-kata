<?php

declare(strict_types=1);

namespace RomanNumeralsKata;

use LogicException;

class Converter
{
    private const MAP = [
        'M' => 1000,
        'CM' => 900,
        'D' => 500,
        'CD' => 400,
        'C' => 100,
        'XC' => 90,
        'L' => 50,
        'XL' => 40,
        'X' => 10,
        'IX' => 9,
        'V' => 5,
        'IV' => 4,
        'I' => 1
    ];

    public function arabicToRoman(int $arabic): string
    {
        if ($arabic > 3000) {
            throw new LogicException("Out of range, please use integer <= 3000 for successful converting");
        }

        $out = '';
        while ($arabic > 0) {
            foreach (self::MAP as $roman => $int) {
                if ($arabic >= $int) {
                    $arabic -= $int;
                    $out .= $roman;
                    break;
                }
            }
        }

        return $out;
    }

    public function arabicToRoman2(int $arabic): string
    {
        $roman = '';
        for ($i = 0; $i < $arabic; $i++) {
            $roman .= 'I';
        }

        $roman = str_replace('IIIII', 'V', $roman);
        $roman = str_replace('IIII', 'IV', $roman);
        $roman = str_replace('VV', 'X', $roman);
        $roman = str_replace('VIV', 'IX', $roman);
        $roman = str_replace('XXXXX', 'L', $roman);
        $roman = str_replace('XXXX', 'XL', $roman);
        $roman = str_replace('LL', 'C', $roman);
        $roman = str_replace('LXL', 'XC', $roman);
        $roman = str_replace('CCCCC', 'D', $roman);
        $roman = str_replace('CCCC', 'CD', $roman);
        $roman = str_replace('DD', 'M', $roman);
        $roman = str_replace('DCD', 'CM', $roman);

        return $roman;
    }

    public function romanToArabic2(string $roman): int
    {
        $roman = str_replace('CM', 'DCD', $roman);
        $roman = str_replace('M', 'DD', $roman);
        $roman = str_replace('CD', 'CCCC', $roman);
        $roman = str_replace('D', 'CCCCC', $roman);
        $roman = str_replace('XC', 'LXL', $roman);
        $roman = str_replace('C', 'LL', $roman);
        $roman = str_replace('XL', 'XXXX', $roman);
        $roman = str_replace('L', 'XXXXX', $roman);
        $roman = str_replace('X', 'VV', $roman);
        $roman = str_replace('IX', 'VIV', $roman);
        $roman = str_replace('IV', 'IIII', $roman);
        $roman = str_replace('V', 'IIIII', $roman);

        return strlen($roman);
    }

}