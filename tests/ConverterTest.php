<?php

declare(strict_types=1);

namespace RomanNumeralsKataTests;

use PHPUnit\Framework\TestCase;
use RomanNumeralsKata\Converter;
use LogicException;

class ConverterTest extends TestCase
{
    /**
     * @dataProvider provider
     */
    public function testArabicToRoman($arabic, $roman): void
    {
        $converter = new Converter();

        self::assertSame($roman, $converter->arabicToRoman($arabic));
    }

    /**
     * @dataProvider provider
     */
    public function testArabicToRoman2($arabic, $roman): void
    {
        $converter = new Converter();

        self::assertSame($roman, $converter->arabicToRoman2($arabic));
    }

    /**
     * @dataProvider provider
     */
    public function testRomanToArabic2($arabic, $roman): void
    {
        $converter = new Converter();

        self::assertSame($arabic, $converter->romanToArabic2($roman));
    }

    public function provider(): array
    {
        return [
            [1, 'I'],
            [2, 'II'],
            [3, 'III'],
            [4, 'IV'],
            [5, 'V'],
            [6, 'VI'],
            [7, 'VII'],
            [8, 'VIII'],
            [9, 'IX'],
            [10, 'X'],
            [11, 'XI'],
            [12, 'XII'],
            [13, 'XIII'],
            [14, 'XIV'],
            [15, 'XV'],
            [16, 'XVI'],
            [17, 'XVII'],
            [18, 'XVIII'],
            [19, 'XIX'],
            [20, 'XX'],
            [21, 'XXI'],
            [24, 'XXIV'],
            [29, 'XXIX'],
            [30, 'XXX'],
            [34, 'XXXIV'],
            [39, 'XXXIX'],
            [49, 'XLIX'],
            [54, 'LIV'],
            [69, 'LXIX'],
            [75, 'LXXV'],
            [89, 'LXXXIX'],
            [99, 'XCIX'],
            [134, 'CXXXIV'],
            [199, 'CXCIX'],
            [256, 'CCLVI'],
            [289, 'CCLXXXIX'],
            [499, 'CDXCIX'],
            [812, 'DCCCXII'],
            [999, 'CMXCIX'],
            [1222, 'MCCXXII'],
            [1348, 'MCCCXLVIII'],
            [1453, 'MCDLIII'],
            [1492, 'MCDXCII'],
            [1499, 'MCDXCIX'],
            [1516, 'MDXVI'],
            [1649, 'MDCXLIX'],
            [1701, 'MDCCI'],
            [1789, 'MDCCLXXXIX'],
            [1812, 'MDCCCXII'],
            [1815, 'MDCCCXV'],
            [1848, 'MDCCCXLVIII'],
            [1879, 'MDCCCLXXIX'],
            [1901, 'MCMI'],
            [1916, 'MCMXVI'],
            [1923, 'MCMXXIII'],
            [1949, 'MCMXLIX'],
            [1957, 'MCMLVII'],
            [1999, 'MCMXCIX'],
            [2499, 'MMCDXCIX'],
            [2749, 'MMDCCXLIX'],
            [3000, 'MMM'],
        ];
    }

    public function testOutOfRange(): void
    {
        $converter = new Converter();

        $this->expectException(LogicException::class);

        $converter->arabicToRoman(3001);
    }
}