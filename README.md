# Roman Numerals Kata

The Kata says you should write a function to convert from
normal (arabic) numbers to Roman numerals:

    1 --> I
    10 --> X
    7 --> VII

See https://codingdojo.org/kata/RomanNumerals/ for more
details.

## Getting started

Download and install phive, see: https://phar.io/

Then install composer

    phive install composer
    chmod a+x tools/composer

Run composer

    composer install

